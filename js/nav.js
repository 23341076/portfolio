window.addEventListener("load", function(){
  var open = document.getElementById('open-menu');
  var close = document.getElementById('close-menu');
  var nav = document.getElementById('main-nav');

  document.getElementById('open-menu').onclick = function(e){
    e.preventDefault();
    nav.className = "show";
  };

  close.onclick = function(e){
    e.preventDefault();
    nav.className = "";
  };
});
